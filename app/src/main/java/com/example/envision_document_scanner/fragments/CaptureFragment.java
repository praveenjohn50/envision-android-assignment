package com.example.envision_document_scanner.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Matrix;
import android.os.Bundle;
import android.os.Environment;
import android.speech.tts.TextToSpeech;
import android.util.Rational;
import android.util.Size;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.camera.core.CameraX;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureConfig;
import androidx.camera.core.Preview;
import androidx.camera.core.PreviewConfig;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;

import com.example.envision_document_scanner.R;
import com.example.envision_document_scanner.presenter.CapturePresenter;
import com.example.envision_document_scanner.presenter.CapturePresenterImpl;
import com.example.envision_document_scanner.view.CaptureView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Praveen John on 07,November,2021
 */

public class CaptureFragment extends Fragment implements CaptureView, View.OnClickListener {

    private int REQUEST_CODE_PERMISSIONS = 101;
    private final String[] REQUIRED_PERMISSIONS
            = new String[]{"android.permission.CAMERA", "android.permission.WRITE_EXTERNAL_STORAGE"};
    TextureView textureView;
    View mView;
    ImageView imageButton, reload;
    ConstraintLayout cameraView;
    CapturePresenter mCapturePresenter;
    TextView mTextView;
    TextToSpeech textToSpeech;
    File mCurrentFile;
    ProgressDialog progressDiaog;
    ImageCapture imgCap;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_capture, container, false);
        return mView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        textureView = view.findViewById(R.id.view_finder);
        imageButton = view.findViewById(R.id.imgCapture);
        cameraView = view.findViewById(R.id.camera_view);
        reload = view.findViewById(R.id.reload);
        imageButton.setOnClickListener(this);
        reload.setOnClickListener(this);
        mTextView = view.findViewById(R.id.textShow);
        mCapturePresenter = new CapturePresenterImpl(this);
        mCapturePresenter.initialize(getContext());
        progressDiaog = new ProgressDialog(getContext());
        progressDiaog.setTitle("Processing Document");
        progressDiaog.setCancelable(false);
        progressDiaog.setMessage("Loading....");
        textToSpeech = new TextToSpeech(getContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    textToSpeech.setLanguage(Locale.UK);
                }
            }
        });

        permissioncheck();
    }

    private void permissioncheck() {
        if (allPermissionsGranted()) {
            startCamera();
        } else {
            ActivityCompat.requestPermissions(getActivity(), REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS);
        }
    }

    private void startCamera() {
        if (progressDiaog.isShowing()) {
            progressDiaog.dismiss();
        }
        mTextView.setText("");

        CameraX.unbindAll();

        Rational aspectRatio = new Rational(textureView.getWidth(), textureView.getHeight());
        Size screen = new Size(textureView.getWidth(), textureView.getHeight()); //size of the screen

        PreviewConfig pConfig = new PreviewConfig.Builder().setTargetAspectRatio(aspectRatio).setTargetResolution(screen).build();
        Preview preview = new Preview(pConfig);

        preview.setOnPreviewOutputUpdateListener(
                new Preview.OnPreviewOutputUpdateListener() {
                    @Override
                    public void onUpdated(Preview.PreviewOutput output) {
                        ViewGroup parent = (ViewGroup) textureView.getParent();
                        parent.removeView(textureView);
                        parent.addView(textureView, 0);
                        textureView.setSurfaceTexture(output.getSurfaceTexture());
                        updateTransform();
                    }
                });


        ImageCaptureConfig imageCaptureConfig = new ImageCaptureConfig.Builder().setCaptureMode(ImageCapture.CaptureMode.MIN_LATENCY)
                .setTargetRotation(getActivity().getWindowManager().getDefaultDisplay().getRotation()).build();
        imgCap = new ImageCapture(imageCaptureConfig);


        CameraX.bindToLifecycle((LifecycleOwner) this, preview, imgCap);
    }


    private void updateTransform() {
        Matrix mx = new Matrix();
        float w = textureView.getMeasuredWidth();
        float h = textureView.getMeasuredHeight();
        float cX = w / 2f;
        float cY = h / 2f;
        int rotationDgr;
        int rotation = (int) textureView.getRotation();
        switch (rotation) {
            case Surface.ROTATION_0:
                rotationDgr = 0;
                break;
            case Surface.ROTATION_90:
                rotationDgr = 90;
                break;
            case Surface.ROTATION_180:
                rotationDgr = 180;
                break;
            case Surface.ROTATION_270:
                rotationDgr = 270;
                break;
            default:
                return;
        }

        mx.postRotate((float) rotationDgr, cX, cY);
        textureView.setTransform(mx);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                startCamera();
            } else {
                Toast.makeText(getContext(), "Permissions not granted by the user.", Toast.LENGTH_SHORT).show();
                //finish();
            }
        }
    }

    private boolean allPermissionsGranted() {

        for (String permission : REQUIRED_PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(getContext(), permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void onResponse(String scannedText) {
        if (progressDiaog.isShowing()) {
            progressDiaog.dismiss();
        }
        customAlert(scannedText);

    }

    @Override
    public void onPause() {
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
        super.onPause();
    }

    public void processDocument() {
        mCapturePresenter.postImage(mCurrentFile);
    }

    private void showAlert() {
        new AlertDialog.Builder(mView.getContext())
                .setTitle("Document Captured")
                .setMessage("DO you want to process the image?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        progressDiaog.show();
                        processDocument();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (progressDiaog.isShowing()) {
                            progressDiaog.dismiss();
                        }
                        startCamera();
                    }
                })
                .setIcon(android.R.drawable.ic_menu_camera)
                .show();
    }


    private void customAlert(String text) {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View promptView = layoutInflater.inflate(R.layout.layout_custom_alert, null);

        final AlertDialog alertD = new AlertDialog.Builder(getContext()).create();
        alertD.setCancelable(false);

        TextView mScannedText = (TextView) promptView.findViewById(R.id.text_passage);
        mScannedText.setText(text);

        Button saveToLibraryButton = (Button) promptView.findViewById(R.id.savetoLibrary);
        saveToLibraryButton.setVisibility(View.VISIBLE);
        saveToLibraryButton.setContentDescription("Save to library Button");

        Button mSpeakButton = (Button) promptView.findViewById(R.id.Speak);
        mSpeakButton.setContentDescription("Speak button");

        saveToLibraryButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mCapturePresenter.savedataToLibrary();
                saveToLibraryButton.setText("Saved to Library");
                alertD.dismiss();
                if (textToSpeech != null) {
                    textToSpeech.stop();
                    textToSpeech.shutdown();
                }
                Toast.makeText(getContext(), "Saved to Library", Toast.LENGTH_SHORT).show();

            }
        });

        mSpeakButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (textToSpeech.isSpeaking()) {
                    textToSpeech.stop();

                } else {
                    textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null);
                }

            }
        });

        alertD.setView(promptView);
        alertD.show();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && progressDiaog != null) {
            startCamera();
        } else {
            if (textToSpeech != null) {
                if (textToSpeech.isSpeaking()) {
                    textToSpeech.stop();
                }
            }
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgCapture:
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.US);
                Date now = new Date();
                String fileName = formatter.format(now);
                File file = new File(Environment.getExternalStorageDirectory() + "/" + fileName + ".png");
                imgCap.takePicture(file, new ImageCapture.OnImageSavedListener() {
                    @Override
                    public void onImageSaved(@NonNull File file) {
                        mCurrentFile = file;
                        String msg = "Pic captured at " + file.getAbsolutePath();
                        showAlert();
                        CameraX.unbindAll();
                    }

                    @Override
                    public void onError(@NonNull ImageCapture.UseCaseError useCaseError, @NonNull String message, @Nullable Throwable cause) {
                        String msg = "Pic capture failed : " + message;
                        Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
                        startCamera();
                        if (cause != null) {
                            cause.printStackTrace();
                        }
                    }
                });
                break;
            case R.id.reload:
                startCamera();
                break;
        }

    }
}
