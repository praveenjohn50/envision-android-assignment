package com.example.envision_document_scanner.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.envision_document_scanner.R;
import com.example.envision_document_scanner.adapters.MyListAdapter;
import com.example.envision_document_scanner.database.DatabaseHelper;
import com.example.envision_document_scanner.database.Note;
import com.example.envision_document_scanner.interfaces.RecyclerViewItemClickListener;
import com.example.envision_document_scanner.presenter.CapturePresenter;
import com.example.envision_document_scanner.presenter.CapturePresenterImpl;
import com.example.envision_document_scanner.presenter.LibraryPresenter;
import com.example.envision_document_scanner.presenter.LibraryPresenterImpl;
import com.example.envision_document_scanner.view.LibraryView;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Praveen John on 06,November,2021
 */

public class LibraryFragment extends Fragment implements RecyclerViewItemClickListener, LibraryView {
    MyListAdapter adapter;
    ArrayList<Note> arraylist;
    View mView;
    RecyclerView recyclerView;
    TextToSpeech textToSpeech;
    AlertDialog alertD;
    LibraryPresenter mLibraryPresenter;

    public LibraryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_library, container, false);
        return mView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mLibraryPresenter = new LibraryPresenterImpl(this);
        mLibraryPresenter.initialize(getContext());
        updateRecyclerView();
        textToSpeech=new TextToSpeech(getContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    textToSpeech.setLanguage(Locale.UK);
                }
            }
        });
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser){
            if(arraylist!= null && adapter!= null) {
                updateRecyclerView();
            }

        }else {
            //Toast.makeText(getContext(), "gone", Toast.LENGTH_SHORT).show();
        }
    }

    public void updateRecyclerView() {
        recyclerView = (RecyclerView) mView.findViewById(R.id.library_list_view);
        arraylist = mLibraryPresenter.getPassages();
        adapter = new MyListAdapter(arraylist, getContext());
        adapter.setListener(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
    }

    private void customAlert(String text) {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View promptView = layoutInflater.inflate(R.layout.layout_custom_alert, null);

        alertD = new AlertDialog.Builder(getContext()).create();
        alertD.setCancelable(false);

        TextView mScannedText = (TextView) promptView.findViewById(R.id.text_passage);
        mScannedText.setText(text);

        Button cancelButon = (Button) promptView.findViewById(R.id.savetoLibrary);
        cancelButon.setText("Back");
        cancelButon.setContentDescription("Back");

        Button mSpeakButton = (Button) promptView.findViewById(R.id.Speak);
        mSpeakButton.setContentDescription("Speak");

        cancelButon.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                alertD.dismiss();
                if(textToSpeech !=null){
                    textToSpeech.stop();
                    textToSpeech.shutdown();
                }

            }
        });

        mSpeakButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(textToSpeech.isSpeaking()) {
                    textToSpeech.stop();

                } else {
                    textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null);
                }

            }
        });

        alertD.setView(promptView);

        alertD.show();
    }

    @Override
    public void inItemClicked(String text) {
        customAlert(text);
    }

    @Override
    public void onPause() {
        super.onPause();
        if(textToSpeech!= null) {
            if (textToSpeech.isSpeaking()) {
                textToSpeech.stop();

            }
        }
    }


    @Override
    public void initialize(Context context) {

    }
}
