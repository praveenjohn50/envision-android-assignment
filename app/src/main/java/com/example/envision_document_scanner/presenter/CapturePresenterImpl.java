package com.example.envision_document_scanner.presenter;

import android.content.Context;

import com.example.envision_document_scanner.model.CaptureModel;
import com.example.envision_document_scanner.model.CaptureModelImpl;
import com.example.envision_document_scanner.view.CaptureView;

import java.io.File;

/**
 * Created by Praveen John on 06,November,2021
 */

public class CapturePresenterImpl implements CapturePresenter {

    CaptureModel mCaptureModel;
    CaptureView mCaptureView;

    public CapturePresenterImpl(CaptureView captureView) {
        mCaptureModel = new CaptureModelImpl(this);
        if (captureView != null) {
            mCaptureView = captureView;
        }

    }

    @Override
    public void initialize(Context context) {
        mCaptureModel.initialize(context);

    }

    @Override
    public void postImage(File file) {
        mCaptureModel.postImage(file);
    }

    @Override
    public void onResponse(String scannedText) {
        mCaptureView.onResponse(scannedText);
    }

    @Override
    public void savedataToLibrary() {
        mCaptureModel.saveDataToLibrary();
    }
}
