package com.example.envision_document_scanner.presenter;

import android.content.Context;

import java.io.File;

/**
 * Created by Praveen John on 06,November,2021
 */

public interface CapturePresenter {
    /**
     * initialize
     */
    void initialize(Context context);

    void postImage(File file);

    void onResponse(String scannedText);
    void savedataToLibrary();
}
