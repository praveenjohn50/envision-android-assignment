package com.example.envision_document_scanner.presenter;

import android.content.Context;

import com.example.envision_document_scanner.database.Note;
import com.example.envision_document_scanner.model.CaptureModel;
import com.example.envision_document_scanner.model.CaptureModelImpl;
import com.example.envision_document_scanner.model.LibraryModel;
import com.example.envision_document_scanner.model.LibraryModelImpl;
import com.example.envision_document_scanner.view.CaptureView;
import com.example.envision_document_scanner.view.LibraryView;

import java.util.ArrayList;

public class LibraryPresenterImpl implements LibraryPresenter{

    LibraryModel mLibraryModel;
    LibraryView mLibraryView;

    public LibraryPresenterImpl(LibraryView libraryView) {
        mLibraryModel = new LibraryModelImpl(this);
        if (libraryView != null) {
            mLibraryView = libraryView;
        }
    }

    @Override
    public void initialize(Context context) {
        mLibraryModel.initialize(context);

    }

    @Override
    public ArrayList<Note> getPassages() {
        return mLibraryModel.getPassages();
    }
}
