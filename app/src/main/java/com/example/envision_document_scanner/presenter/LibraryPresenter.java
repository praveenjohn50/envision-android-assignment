package com.example.envision_document_scanner.presenter;

import android.content.Context;

import com.example.envision_document_scanner.database.Note;

import java.util.ArrayList;

/**
 * Created by Praveen John on 06,November,2021
 */

public interface LibraryPresenter {
    /**
     * initialize
     */
    void initialize(Context context);

    ArrayList<Note> getPassages();
}
