package com.example.envision_document_scanner.interfaces;

/**
 * Created by Praveen John on 07,November,2021
 */

public interface RecyclerViewItemClickListener {
    void inItemClicked(String text);
}
