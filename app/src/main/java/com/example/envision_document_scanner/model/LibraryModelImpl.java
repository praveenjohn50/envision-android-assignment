package com.example.envision_document_scanner.model;

import android.content.Context;

import com.example.envision_document_scanner.database.DatabaseHelper;
import com.example.envision_document_scanner.database.Note;
import com.example.envision_document_scanner.presenter.CapturePresenter;
import com.example.envision_document_scanner.presenter.LibraryPresenter;
import com.example.envision_document_scanner.presenter.LibraryPresenterImpl;

import java.io.File;
import java.util.ArrayList;

public class LibraryModelImpl implements LibraryModel {
    LibraryPresenter mLibraryPresenter;
    Context mContext;
    DatabaseHelper databaseHelper;


    public LibraryModelImpl(LibraryPresenterImpl libraryPresenter) {
        if (libraryPresenter != null) {
            mLibraryPresenter = libraryPresenter;
        };
    }

    @Override
    public void initialize(Context context) {
        mContext = context;
        databaseHelper = new DatabaseHelper(context);
    }

    @Override
    public ArrayList<Note> getPassages() {
        return databaseHelper.getAllPassage();
    }
}
