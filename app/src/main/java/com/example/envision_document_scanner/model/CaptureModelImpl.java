package com.example.envision_document_scanner.model;

import static androidx.constraintlayout.motion.utils.Oscillator.TAG;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.envision_document_scanner.ConstantValues;
import com.example.envision_document_scanner.MainActivity;
import com.example.envision_document_scanner.database.DatabaseHelper;
import com.example.envision_document_scanner.database.Note;
import com.example.envision_document_scanner.presenter.CapturePresenter;
import com.example.envision_document_scanner.retrofit.APIResponse;
import com.example.envision_document_scanner.retrofit.DataModal;
import com.example.envision_document_scanner.retrofit.Paragraph;
import com.example.envision_document_scanner.retrofit.RetrofitAPI;
import com.example.envision_document_scanner.retrofit.Root;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Praveen John on 06,November,2021
 */

public class CaptureModelImpl implements CaptureModel {
    CapturePresenter mCapturePresenter;
    Context mContext;
    DatabaseHelper databaseHelper;
    Note mCurrentNote;

    @Override
    public void initialize(Context context) {
        mContext = context;
        databaseHelper = new DatabaseHelper(context);

    }

    @Override
    public void postImage(File file) {
        postData(file);
    }

    @Override
    public void saveDataToLibrary() {
        databaseHelper.insertData(mCurrentNote.getFileName(), mCurrentNote.getLanguage(), mCurrentNote.getPassage());
    }

    public CaptureModelImpl(CapturePresenter capturePresenter) {
        if (capturePresenter != null) {
            mCapturePresenter = capturePresenter;
        }

    }

    private void postData(File file) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ConstantValues.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RetrofitAPI retrofitAPI = retrofit.create(RetrofitAPI.class);

        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);

        MultipartBody.Part body =
                MultipartBody.Part.createFormData("photo", file.getName(), requestFile);
        Log.d(TAG, "postData: " + body.toString());


        Call<Root> call = retrofitAPI.uploadReceipt(ConstantValues.COOKIE, body);

        call.enqueue(new Callback<Root>() {
            @Override
            public void onResponse(Call<Root> call, Response<Root> response) {
                StringBuffer sb = new StringBuffer();

                Root responseFromAPI = response.body();
                String language = responseFromAPI.getResponse().getParagraphs().get(0).language;
                List<Paragraph> arrayparra = responseFromAPI.getResponse().getParagraphs();
                for (int i = 0; i < arrayparra.size(); i++) {
                    sb.append(arrayparra.get(i).paragraph);
                    sb.append(System.lineSeparator());

                }
                Log.d(TAG, "onResponse: " + sb);
                mCapturePresenter.onResponse(sb.toString());
                mCurrentNote = new Note();
                mCurrentNote.setLanguage(language);
                mCurrentNote.setFileName(file.getName());
                mCurrentNote.setPassage(sb.toString());


            }

            @Override
            public void onFailure(Call<Root> call, Throwable t) {
                Log.d(TAG, "onResponse: error");
            }
        });
    }
}
