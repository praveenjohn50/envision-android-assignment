package com.example.envision_document_scanner.model;

import android.content.Context;

import java.io.File;

/**
 * Created by Praveen John on 06,November,2021
 */

public interface CaptureModel {

    /**
     * initialize
     */
    void initialize(Context context);

    void postImage(File file);
    void saveDataToLibrary();
}
