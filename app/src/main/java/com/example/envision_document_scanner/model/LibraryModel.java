package com.example.envision_document_scanner.model;

import android.content.Context;

import com.example.envision_document_scanner.database.Note;

import java.util.ArrayList;

/**
 * Created by Praveen John on 06,November,2021
 */

public interface LibraryModel {
    /**
     * initialize
     */
    void initialize(Context context);

    ArrayList<Note> getPassages();
}
