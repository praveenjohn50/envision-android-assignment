package com.example.envision_document_scanner.database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Praveen John on 06,November,2021
 */

public class DatabaseHelper extends SQLiteOpenHelper {


    private static final int DATABASE_VERSION = 1;  //Version of the current database
    private static final String DATABASE_NAME = "sample_database"; //Name of the database


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    //Creates a new Database
    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("Create_table_syntax", Note.CREATE_TABLE);
        db.execSQL(Note.CREATE_TABLE); //To Create a table in database
    }


    //Code to upgrade the database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + Note.TABLE_NAME);
        onCreate(db);
    }


    public long insertData(String fileName, String language, String passage) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Note.COLUMN_PASSAGE_LANGUAGE, language);
        values.put(Note.COLUMN_FILE_NAME, fileName);
        values.put(Note.COLUMN_PASSAGE, passage);
        long id = db.insertWithOnConflict(Note.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_IGNORE);
        db.close();
        return id;
    }

    @SuppressLint("Range")
    public ArrayList<Note> getAllPassage() {
        ArrayList<Note> notes = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + Note.TABLE_NAME + " ORDER BY " +
                Note.COLUMN_ID + " ASC";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Note note = new Note();
                note.setPassid(cursor.getInt(cursor.getColumnIndex(Note.COLUMN_ID)));
                note.setFileName(cursor.getString(cursor.getColumnIndex(Note.COLUMN_FILE_NAME)));
                note.setLanguage(cursor.getString(cursor.getColumnIndex(Note.COLUMN_PASSAGE_LANGUAGE)));
                note.setPassage(cursor.getString(cursor.getColumnIndex(Note.COLUMN_PASSAGE)));

                notes.add(note);
            } while (cursor.moveToNext());
        }
        db.close();
        return notes;
    }


}