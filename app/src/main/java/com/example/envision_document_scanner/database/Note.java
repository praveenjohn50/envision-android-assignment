package com.example.envision_document_scanner.database;

/**
 * Created by Praveen John on 06,November,2021
 */

public class Note {


    public Note() {
    }


    public static final String TABLE_NAME = "passage_table";         //Name of the Table
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_FILE_NAME = "file_name";
    public static final String COLUMN_PASSAGE_LANGUAGE = "topic";
    public static final String COLUMN_PASSAGE = "passage";


    int passid;
    String fileName;

    public int getPassid() {
        return passid;
    }

    public void setPassid(int passid) {
        this.passid = passid;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getPassage() {
        return passage;
    }

    public void setPassage(String passage) {
        this.passage = passage;
    }

    String language;
    String passage;


    public Note(int id, String fileName, String language, String passage) {
        this.language = language;
        this.passage = passage;
        this.fileName = fileName;
    }

    // Create table SQL query
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_FILE_NAME + " TEXT,"
                    + COLUMN_PASSAGE_LANGUAGE + " TEXT,"
                    + COLUMN_PASSAGE + " TEXT UNIQUE"
                    + ")";


}