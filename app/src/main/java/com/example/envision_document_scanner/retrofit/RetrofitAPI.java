package com.example.envision_document_scanner.retrofit;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by Praveen John on 06,November,2021
 */

public interface RetrofitAPI {


    @Multipart
    @POST("/api/test/readDocument")
    Call<Root> uploadReceipt(
            @Header("Cookie") String sessionIdAndRz,
            @Part MultipartBody.Part file

    );
}