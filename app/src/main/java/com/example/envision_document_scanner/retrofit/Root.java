package com.example.envision_document_scanner.retrofit;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Praveen John on 06,November,2021
 */

public class Root implements Serializable {
    public APIResponse getResponse() {
        return APIResponse;
    }

    public void setResponse(APIResponse APIResponse) {
        this.APIResponse = APIResponse;
    }

    @SerializedName("response")
    public APIResponse APIResponse;
}