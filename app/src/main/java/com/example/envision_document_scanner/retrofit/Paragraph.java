package com.example.envision_document_scanner.retrofit;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Praveen John on 06,November,2021
 */

public class Paragraph implements Serializable {
    public String getParagraph() {
        return paragraph;
    }

    public void setParagraph(String paragraph) {
        this.paragraph = paragraph;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @SerializedName("paragraph")
    public String paragraph;
    @SerializedName("language")
    public String language;
}