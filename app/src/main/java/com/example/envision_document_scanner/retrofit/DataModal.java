package com.example.envision_document_scanner.retrofit;

import java.io.File;

/**
 * Created by Praveen John on 06,November,2021
 */

public class DataModal {

    // string variables for our name and job
    private File mFile;

    public DataModal(File file) {
        this.mFile = file;
    }

    public File getmFile() {
        return mFile;
    }

    public void setmFile(File mFile) {
        this.mFile = mFile;
    }
}