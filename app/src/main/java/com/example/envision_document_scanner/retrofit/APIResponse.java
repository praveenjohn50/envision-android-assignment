package com.example.envision_document_scanner.retrofit;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Praveen John on 06,November,2021
 */

public class APIResponse implements Serializable {
    public List<Paragraph> getParagraphs() {
        return paragraphs;
    }

    public void setParagraphs(List<Paragraph> paragraphs) {
        this.paragraphs = paragraphs;
    }

    @SerializedName("paragraphs")
    public List<Paragraph> paragraphs;
}