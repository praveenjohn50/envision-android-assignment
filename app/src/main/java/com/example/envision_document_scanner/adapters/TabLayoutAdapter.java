package com.example.envision_document_scanner.adapters;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.envision_document_scanner.fragments.CaptureFragment;
import com.example.envision_document_scanner.fragments.LibraryFragment;

/**
 * Created by Praveen John on 06,November,2021
 */

public class TabLayoutAdapter extends FragmentPagerAdapter {

    Context mContext;
    int mTotalTabs;

    public TabLayoutAdapter(Context context, FragmentManager fragmentManager, int totalTabs) {
        super(fragmentManager);
        mContext = context;
        mTotalTabs = totalTabs;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Log.d("getItem", position + "");
        switch (position) {
            case 0:
                return new CaptureFragment();
            case 1:
                return new LibraryFragment();
            default:
                return null;

        }
    }

    @Override
    public int getCount() {
        return mTotalTabs;
    }
}
