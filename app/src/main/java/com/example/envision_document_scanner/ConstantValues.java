package com.example.envision_document_scanner;

/**
 * Created by Praveen John on 06,November,2021
 */

public class ConstantValues {

    public static final String COOKIE = "__cfduid=d97604b6c67574ccd048c013ffbee703a1614774197";
    public static final String BASE_URL = "https://letsenvision.app";
}
