package com.example.envision_document_scanner.view;

import android.content.Context;

/**
 * Created by Praveen John on 06,November,2021
 */

public interface LibraryView {
    /**
     * initialize
     */
    void initialize(Context context);
}
